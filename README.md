# Spinkdistro Web

This is the source code of our website, [spinkdistro.art](spinkdistro.art).


## How to develop

Install node version manager (nvm):
```
sudo apt install curl
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
source ~/.bashrc   
```

This works for Ubuntu-based distros that use the `apt` package manager. Other
distros such as Fedora need to use their own package manager (`dnf` in the case of Fedora).

Install the latest version of node:
```
nvm install node
nvm use node
```

Install npm:
```
sudo apt install npm
```

Intall pnpm:
```
npm install -g pnpm
```

Run the project:
```
pnpm install
pnpm run dev
```

