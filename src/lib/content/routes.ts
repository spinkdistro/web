import type { Routes } from "$lib/types";

export const routes: Routes = {
  '/': {
    cs: 'Zíny',
    en: 'Zines'
  },
  '/odkazy': {
    cs: 'Odkazy',
    en: 'Links'
  },
  '/navody': {
    cs: 'Návody',
    en: 'Tutorials'
  }
};
