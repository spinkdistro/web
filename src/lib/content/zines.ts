import type { ZineList } from '../types.ts';

const PDF_ROOT_URL = "https://gitlab.com/spinkdistro/Zines/-/raw/main/";

export function t(str: TemplateStringsArray) {
	let result = '';

	for (let i = 0; i < str.length; i++) {
		result += str[i];
	}

	result = result
		.split('\n')
		.map((row) => row.trim())
		.join('\n');

	if (result.startsWith('\n')) {
		result = result.slice(1);
	}

	result = result
		.replace(/^\s*[\r\n]/gm, 'XXXX') //replace empty lines with XXXX
		.replace(/(.)\n/g, '$1 ')
		.replace(/\n/g, '') //replace \n with a space
		.replace(/\.\s($|\n)/g, '.$1') //\. \n with \.\n
		.replace(/XXXX/g, '\n\n'); //replace XXXX with two empty lines

	//console.log(result);

	return result;
}

export const zines: ZineList = {
	cs: {
		bio: {
			name: 'Biocentrická anarchie',
			author: 'Anonymní',
			version: '1.1.4',

			thumbnail: '/thumbnails/cs/small/bio.png',
			thumbnailLarge: '/thumbnails/cs/large/bio.png',
			thumbnailAlt:
				'Selfie makaka smějícího se do kamery. Nápis biocentrická anarchie a anarchistické A v kruhu.',

			summary: t`
				Zvířata, rostliny a příroda: nachází se kolem nás a tvoří
				velkou část našeho života. Podstata této tvorby však není
				spolupráce: místo života se zvířaty jde spíše o život nad zvířaty.

				Nahlížení na vše ne-lidské jako „nižší“ a pouze určené
				k vykořisťování se nazývá antropocentrismus a (dle
				intersekcionální teorie) je takové myšlení napojeno na
				všechny jiné systémy útlaku.

				Autorka zde nabádá k překonání antropocentrismu a vykročení
				směrem k něčemu jinému, biocentrismu (a anarchismu,
				pokud bojujeme i proti všem ostatním systémům útlaku).
			`,

			downloads: [
				{
					name: 'A5 pro čtení (PDF)',
					link: PDF_ROOT_URL + 'cs/BiocentrickaAnarchie/bioA5.pdf'
				},
				{
					name: 'A5 brožurka pro tisk na A4 (PDF)',
					link: PDF_ROOT_URL + 'cs/BiocentrickaAnarchie/bioA5BookletA4.pdf'
				}
			],
		},

		patriarchat: {
			name: 'Porozumění patriarchátu',
			author: 'bell hooks',
			version: '1.0.2',

			thumbnail: '/thumbnails/cs/small/patriarchat.png',
			thumbnailLarge: '/thumbnails/cs/large/patriarchat.png',
			thumbnailAlt:
				'Porozumění patriarchátu, bell hooks. Na pozadí kubistický obraz černé rodiny s otcem, matkou a dvěma dětmi.',

			summary: t`
				Proč některé děti nesmějí hrát kuličky? Proč mají někteří lidé potřebu
				nad jinými neustále panovat a rozkazovat jim? A proč se pojmenování
				tohoto problému ve společnosti setkává s odporem?

				Žijeme v patriarchátu: systému genderované nadvlády. bell hooks na
				příkladu svého dospívání vysvětluje, jak patriarchát funguje a jak
				přispívá k výše zmíněným problémům.
			`,

			downloads: [
				{
					name: 'A5 pro čtení (PDF)',
					link: PDF_ROOT_URL + 'cs/PorozumeniPatriarchatu/patriarchatA5.pdf'
				},
				{
					name: 'A5 brožurka pro tisk na A4 (PDF)',
					link: PDF_ROOT_URL + 'cs/PorozumeniPatriarchatu/patriarchatA5BookletA4.pdf'
				},
				{
					name: 'Doplněk: feministický slovníček A4 pro čtení (PDF)',
					link: PDF_ROOT_URL + 'cs/PorozumeniPatriarchatu/slovnicekA4.pdf'
				},
				{
					name: 'Doplněk: feministický slovníček A5 brožurka pro tisk na A4 (PDF)',
					link: PDF_ROOT_URL + 'cs/PorozumeniPatriarchatu/slovnicekA5BookletA4.pdf'
				}
			]
		},

		znic: {
			name: 'Znič gender',
			author: 'Lena Kafka',
			version: '1.2.0',

			thumbnail: '/thumbnails/cs/small/znic.png',
			thumbnailLarge: '/thumbnails/cs/large/znic.png',
			thumbnailAlt: 'Člověk s pistolí na ulici, text znič gender.',

			summary: t`
				Naše povstání proti genderu nemůže skončit jen u genderové
				sebeidentifikace nebo nového seznamu pojmů, který se všechny
				naučí respektovat. Povstání se musí posunout za tyto hranice
				k volné hře jednání, chování, sexuality atd. Kde konání nebo
				užívání si té či oné činnosti nezařazuje jedince do omezující
				role.

				Být svobodná od vlády znamená být svobodná od genderu. Osvobození
				od genderu znamená zbavení se kategorizace, normalizace
				a vykořisťování nadvlády.
		  `,

			downloads: [
				{
					name: 'A4 pro čtení (PDF)',
					link: PDF_ROOT_URL + 'cs/ZnicGender/znicA4.pdf'
				},
				{
					name: 'A6 pro čtení (PDF)',
					link: PDF_ROOT_URL + 'cs/ZnicGender/znicA6.pdf'
				},
				{
					name: 'A5 brožurka pro tisk na A4 (PDF)',
					link: PDF_ROOT_URL + 'cs/ZnicGender/znicA5BookletA4.pdf'
				},
				{
					name: 'A6 brožurka pro tisk na A4 (PDF)',
					link: PDF_ROOT_URL + 'cs/ZnicGender/znicA6BookletA4.pdf'
				},
			],
		},

		zruseni: {
			name: 'Zrušení práce',
			author: 'Bob Black',
			version: '1.2.2',

			thumbnail: '/thumbnails/cs/small/zruseni.png',
			thumbnailLarge: '/thumbnails/cs/large/zruseni.png',
			thumbnailAlt: 'Dva lidé spokojeně ležící pod peřinou hlavami k sobě. Anarchistické A.',

			summary: t`
				Práce, či robota: výroba zboží či služeb vynucená silou či jinými
				prostředky. Všechna z nás robotujeme (jestli se necítíte nucena,
				zkuste do práce nebo do školy na týden nepřijít) – a není na tom
				pranic cnostného. Měla bychom se snažit vytvořit společnost bez práce,
				zatím si ale život bez práce ani nedokážeme představit.

				Bob Black popisuje zakořeněnost práce v naší imaginaci a praxi a
				navrhuje jiný svět: místo práce je však založen na hře.
		  `,

			downloads: [
				{
					name: 'A4 pro čtení (PDF)',
					link: PDF_ROOT_URL + 'cs/ZruseniPrace/zruseniA4.pdf'
				},
				{
					name: 'A5 brožurka pro tisk na A4 (PDF)',
					link: PDF_ROOT_URL + 'cs/ZruseniPrace/zruseniA5BookletA4.pdf'
				}
			],
		},

		law: {
			name: 'Život bez zákonů',
			author: 'Strangers in a Tangled Wilderness',
			version: '1.1.3',

			thumbnail: '/thumbnails/cs/small/law.png',
			thumbnailLarge: '/thumbnails/cs/large/law.png',
			thumbnailAlt: 'Život bez zákonů, aneb úvod do anarchistické politiky. Anarchistické A.',

			summary: t`
				Anarchista*ka je někdo, kdo odmítá nadvládu jedné osoby
				nad druhou nebo jedné skupiny lidí nad druhou. Anarchismus
				je velmi široký zastřešující termín pro skupinu politických
				filosofií, které jsou založené na myšlence, že můžeme žít jako
				anarchisté*ky. My, anarchisté*ky, chceme svět bez národů, vlád,
				kapitalismu, rasismu, sexismu, homofobie... bez žádného z těch
				početných systémů nadvlády, jejichž tíhu nese svět dneška.

				Není žádné jedno perfektní vyjádření anarchismu, protože anarchismus
				je síť myšlenek namísto jedné dogmatické filosofie. A dost nám to
				takhle vyhovuje.
			`,


			downloads: [
				{
					name: 'A5 pro čtení (PDF)',
					link: PDF_ROOT_URL + 'cs/ZivotBezZakonu/lawA5.pdf'
				},
				{
					name: 'A5 brožurka pro tisk na A4 (PDF)',
					link: PDF_ROOT_URL + 'cs/ZivotBezZakonu/lawA5BookletA4.pdf'
				}
			],

		},

		osvobozenou: {
			name: 'Pro osvobozenou akademii',
			author: 'jindrx',
			version: '0.1.0',

			thumbnail: '/thumbnails/cs/small/osvobozenou.png',
			thumbnailLarge: '/thumbnails/cs/large/osvobozenou.png',
			thumbnailAlt: 'Pro osvobozenou akademii',

			summary: t`
				-
		  `,

			downloads: [
				{
					name: 'A4 pro čtení (PDF)',
					link: PDF_ROOT_URL + 'cs/ProOsvobozenouAkademii/osvobozeneA4.pdf'
				},
				{
					name: 'A5 brožurka pro tisk na A4 (PDF)',
					link: PDF_ROOT_URL + 'cs/ProOsvobozenouAkademii/osvobozeneA5BookletA4.pdf'
				}
			],
		},

		vztah: {
			name: 'Manifest vztahové anarchie',
			collaboration: true,
			author: 'Andie Norgen',
			version: '1.0.0',

			thumbnail: '/thumbnails/cs/small/vztah.png',
			thumbnailLarge: '/thumbnails/cs/large/vztah.png',
			thumbnailAlt: 'Manifest vztahové anarchie, Andie Norgen.',

			summary: t`Vztahová anarchie je způsob přemýšlení o tom, jak utváříme vztahy v kontextu současných společenských norem. Snaží se popřít standardy majetnictví a „policajtování“, které pácháme jedna*en na druhé*m ve svých (nejen romantických) vztazích.

			Tento manifest krátce představuje, jak vztahovou anarchii aplikovat do našich vztahů.`,

			downloads: [
				{
					name: 'A6 pro čtení (PDF)',
					link: PDF_ROOT_URL + 'cs/ManifestVztahoveAnarchie/vztahA6.pdf'
				},
				{
					name: 'A6 brožurka pro tisk na A4 (PDF)',
					link: PDF_ROOT_URL + 'cs/ManifestVztahoveAnarchie/vztahA6BookletA4.pdf'
				}
			]
		},
		generibus: {
			name: 'De generibus',
			collaboration: true,
			author: 'Luz',
			version: '1.0.0',

			thumbnail: '/thumbnails/cs/small/generibus.png',
			thumbnailLarge: '/thumbnails/cs/large/generibus.png',
			thumbnailAlt: 'De generibus, osobní zín o genderkách, cis trans a vše ostatní.',

			summary: t` Osobní zín o genderkách, cis trans a vše ostatní. Tento zin
			je mozaika a kaleidoskop různých zkušeností – a ať už jste cis nebo
			trans, doufám, že se na střípky podíváte s respektem pro jejich
			jedinečnost, a třeba v některém zachytíte v odlesku kousek obrazu sebe.

			<strong>Na tvorbě se spinkdistro nepodílelo, zín pouze distribuujeme.</strong>`,

			downloads: [
				{
					name: 'Barevená verze A4 (PDF)',
					link: '/additional_zines/Generibus_color.pdf'
				},
				{
					name: 'Černobílá verze A4 (PDF)',
					link: '/additional_zines/Generibus_bnw.pdf'
				}
			]
		}
	},

	en: {
		law: {
			name: 'Life Without Law',
			author: 'Strangers in a Tangled Wilderness',
			version: '1.0.0',

			thumbnail: '/thumbnails/en/small/law.png',
			thumbnailLarge: '/thumbnails/en/large/law.png',
			thumbnailAlt: 'Life Without Law, an introduction to anarchist politics. Anarchist A.',

			summary: t`
				An anarchist is someone who rejects the domination of one person or class of
				people over another. Anarchism is a very broad umbrella term for a group of
				political philosophies that are based on the idea that we can live as
				anarchists. We anarchists want a world without nations, governments,
				capitalism, racism, sexism, homophobia… without any of the numerous,
				intersecting systems of domination the world bears the weight of today.

				There is no single perfect expression of anarchism, because anarchism is a
				network of ideas instead of a single dogmatic philosophy. And we quite prefer
				it that way.
		  `,

			downloads: [
				{
					name: 'A4 for screen reading (PDF)',
					link: PDF_ROOT_URL + 'en/LifeWithoutLaw/lawA4.pdf'
				},
				{
					name: 'A5 brochure for A4 printing (PDF)',
					link: PDF_ROOT_URL + 'en/LifeWithoutLaw/lawA5BookletA4.pdf'
				}
			],
		}
	}
};
