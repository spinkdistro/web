import { Locale } from "./types";


export const isLocale = (str: unknown): str is Locale => {
  if (typeof str !== 'string') return false;
  const localeValues: string[] = Object.values(Locale);
  return localeValues.includes(str);
}

export const localeOfUrl = (url: URL, preferedLanguages: readonly string[] = []): Locale => {
	const hl = url.searchParams.get('hl');
	if (isLocale(hl)) return hl;
  for (const preferredLanguage of preferedLanguages) {
    const l = preferredLanguage.split('-')[0];
    if (isLocale(l)) return l;
  }
  return Locale.Czech;
};
