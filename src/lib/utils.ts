/* eslint-disable @typescript-eslint/no-namespace */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-non-null-assertion */
import type { Writable as VanillaWritable, Subscriber } from 'svelte/store';

export interface Writable<T> extends VanillaWritable<T> {
	get(): T;
}

export function writable<T>(value?: T): Writable<T> {
	const subs = new Set<Subscriber<T>>();

	return {
		get() {
			return value!;
		},

		set(v) {
			value = v;
		},

		update(fn) {
			value = fn(value!);
		},

		subscribe(sub: Subscriber<T>) {
			subs.add(sub);
			sub(value!);
			return () => subs.delete(sub);
		}
	};
}

export function arrayStoreProxy<T>(arr: Writable<T>[]): T[] {
	return new Proxy([], {
		get(_, i: any) {
			if (typeof i === 'string' && `${+i | 0}` === i) {
				return arr[+i].get();
			}

			return arr[i];
		},

		set(_, i: any, value) {
			if (typeof i === 'string' && `${+i | 0}` === i) {
				arr[+i].set(value);
			} else {
				arr[i] = value;
			}

			return true;
		}
	});
}

/** Dispatch event on click outside of node */
export function clickOutside(node: HTMLElement) {
	const handleClick = (event: MouseEvent) => {
		if (node && !node.contains(event.target as Node) && !event.defaultPrevented) {
			node.dispatchEvent(new CustomEvent('clickOutside', node as any));
		}
	};

	document.addEventListener('click', handleClick, true);

	return {
		destroy() {
			document.removeEventListener('click', handleClick, true);
		}
	};
}
