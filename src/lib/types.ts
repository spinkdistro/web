export enum Locale {
	Czech = 'cs',
	English = 'en'
}

export type LocalizedString = {
	[l in Locale]: string;
};

export interface Routes {
	[path: string]: LocalizedString;
}

export interface Zine {
	name: string;
	author: string;
	version: string;

	collaboration?: true;

	thumbnail: string;
	thumbnailLarge:string;
	thumbnailAlt: string;

	downloads: ZineDownload[];
	summary: string;
}

export interface ZineDownload {
	name: string;
	link: string;
}

export interface Zines {
	[id: string]: Zine;
}

export interface ZineList {
	[lang: string]: Zines;
}