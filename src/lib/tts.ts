export interface AudioSnippet {
	readonly playing: boolean;
	play(): Promise<void>;
	pause(): void;
	stop(): void;
}

const GOOGLE_TTS_URL = 'https://translate.google.com/translate_tts';

export function generateSnippetViaGoogleTranslate(
	text: string,
	lang: string,
	speed = 1
): AudioSnippet {
	const params = new URLSearchParams({
		ie: 'UTF-8',
		q: text,
		tl: lang,
		total: '1',
		idx: '0',
		textlen: text.length.toString(),
		client: 'tw-ob',
		prev: 'input',
		ttsspeed: speed.toString()
	});
	const audioUrl = `${GOOGLE_TTS_URL}?${params}`;
	const audio = new Audio(audioUrl);

	return {
		get playing() {
			return audio.currentTime > 0 && !audio.paused && !audio.ended;
		},

		play: () => audio.play(),
		pause: () => audio.pause(),

		stop: () => {
			audio.pause();
			audio.currentTime = 0;
		}
	};
}
