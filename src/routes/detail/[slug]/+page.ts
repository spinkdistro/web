import { error } from '@sveltejs/kit';
import { zines } from '$lib/content/zines';


export const load = ({ params }) => {
    return {
        slug: params.slug
    };
};
