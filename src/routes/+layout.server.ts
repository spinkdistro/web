import { localeOfUrl } from '$lib/locale';
import type { LayoutServerLoad } from './$types';

export const load = (async ({ url, request: { headers } }) => {
	const preferedLanguages =
		headers
			.get('Accept-Language')
			?.split(',')
			.map((l) => l.trim()) ?? [];

	const locale = localeOfUrl(url, preferedLanguages);

	return {
		locale
	};
}) satisfies LayoutServerLoad;
