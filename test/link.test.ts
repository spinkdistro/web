import { assert } from 'https://deno.land/std@0.192.0/testing/asserts.ts';
import { zines } from '../src/lib/content/zines.ts';

function createAbortController(): AbortController {
	const controller = new AbortController();
	return {
		signal: controller.signal,
		abort: (reason) => controller.abort(reason)
	};
}

const zineList = Object.keys(zines).flatMap((hl) => Object.values(zines[hl]));

Deno.test('links to zines are not dead', async () => {
	let success = true;

	for (const zine of zineList) {
		for (const { link } of zine.downloads) {
			const { signal, abort } = createAbortController();
			const req = await fetch(link, { signal });

			if (!req.ok) {
				success = false;
				console.error(`Failed to fetch "${link}".`);
			}

			abort();
		}
	}

	assert(success, 'Failed to fetch some URLs.');
});
